/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.ox;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class OX {

    char[][] board = new char[3][3];
    int row;
    int col;
    char sign;
    int countDat = 0;

    Scanner kb = new Scanner(System.in);

    public void printBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public void setBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
        printBoard();
    }

    public void turnStatus() {
        //for (int i = 0; i < 9; i++) {
        int i = 0;
        while (true) {
            if (i % 2 == 0) {
                sign = 'O';
            } else {
                sign = 'X';
            }
            System.out.println("Turn " + sign);
            setSign(sign);
            if (checkWin(sign)) {
                System.out.println(">>> " + sign + " Win <<<");
                break;
            } else if (checkDraw()) {
                System.out.println(">>> Draw <<<");
                break;
            }
            i++;
        }
    }

    public void setSign(char sign) {
        System.out.println("Please input row, col: ");
        row = kb.nextInt();
        col = kb.nextInt();
        if (row <= 3 && col <= 3 && board[row-1][col-1] == '-') {
            board[row-1][col-1] = sign;
            printBoard();
            checkWin(sign);
        } else {
            setSign(sign);
        }
    }

    public boolean checkWin(char sign) {
        return checkAllRow(sign) || checkAllCol(sign) || checkDiagonal(sign);
    }

    public boolean checkAllRow(char sign) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if ((board[0][0] == sign && board[0][1] == sign && board[0][2] == sign)
                        || (board[1][0] == sign && board[1][1] == sign && board[1][2] == sign)
                        || (board[2][0] == sign && board[2][1] == sign && board[2][2] == sign)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkAllCol(char sign) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if ((board[0][0] == sign && board[1][0] == sign && board[2][0] == sign)
                        || (board[0][1] == sign && board[1][1] == sign && board[2][1] == sign)
                        || (board[0][2] == sign && board[1][2] == sign && board[2][2] == sign)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean checkDraw() {
        countDat = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if ((board[i][j] == '-')) {
                    countDat++;
                }
            }
        }
        if (countDat == 0) {
            return true;
        }
        return false;
    }

    public boolean checkDiagonal(char sign) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if ((board[0][0] == sign && board[1][1] == sign && board[2][2] == sign)
                        || (board[0][2] == sign && board[1][1] == sign && board[2][0] == sign)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        OX ox = new OX();
        System.out.println("Welcome to OX Game");
        ox.setBoard();
        ox.turnStatus();
    }
}
